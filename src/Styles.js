import React from 'react';
import './App.css';

function Styles2() {
  return (
    <div>
      <h2 style={{ color: "rgb(85, 85, 85)" }}>Building a website the *hard* way.</h2>
      
      <p>In the past year, I've been lucky enough to play around with a mix of cloud infrastructure, modern development processes, frameworks and security best practices associated with these technologies. I've been wanting to create a personal website using a combination these skills. My past attempts have always ended with using a platform like <a href="squarespace.com">Squarespace</a> where I wouldn't feel as satisfied because everything from design, to infrastructure and to security was already done for me.</p>
      <p>As with all engineers... why do things the easy way when I can do it the *hard* way?</p>
      <p>This post is a high-level breakdown of how I built brianl.me from start to finish.</p>
      <p><i>NOTE: This is NOT about how to design a pretty website: the site itself is quite minimal. The focus is on the infrastructure aspects on setting up a website.</i></p>

      <h3 style={{ color: "rgb(85, 85, 85)" }}>Requirements</h3>
      <p>The first step of building something is requirements gathering: what do I actually want out of my website?</p>
      <p><u>Basic requirements</u></p>
      <ul>
        <li>As free as possible :)</li>
        <li>A simple landing page with info "about me" and a list of my projects.</li>
        <li>A page for details of each project, which might include text, images and external links.</li>
        <li>A single-page experience for visitors / no need to reload pages.</li>
        <li>Secure.</li>
        <li>Analytics.</li>
      </ul>

      <p><u>Advanced requirements (for the future)</u></p>
      <ul>
        <li>Hosting internal services.</li>
        <li>Endpoints that would allow me to talk to internal services.</li>
      </ul>

      <h3 style={{ color: "rgb(85, 85, 85)" }}>Design</h3>
      <p>Next, is to plan the design/architecture details of the website.</p>

      <p><u>Storage</u></p>
      <p>Read: Support up to ~1,000 visitors per month.</p>
      <ul>
        <li>Since I'm looking to serve only static pages, Amazon S3 is the best choice for storage.</li>
      </ul>
      <p>Write: There are no features that will support writes (this might change with advanced requirements).</p>

      <p><u>Compute</u></p>
      <p>No computation needs to be performed (this might change with advanced requirements).</p>

      <p><u>Network</u></p>
      <p>I want to keep my network design simple. It's just a personal website. I'm not expecting surges in traffic and no one will miss it while its offline.</p>
      <p>My choice is to use Route 53 for registering my domain name and hosting records. Cloudflare to provide me with CDN services.</p>
      <ul>
        <li><i>1 x availability zone / region</i>: Sydney, Australia. I expect most of my viewers to come from there and would stick with this region unless analytics tell me otherwise.</li>
        <li><i>Load balancing</i>: no.</li>
        <li><i>Caching</i>: yes. People should expect super fast speeds for such a small website.</li>
        <li><i>Denial of service protection</i>: yes. Cloudflare provides this for free.</li>
        <li><i>Web Application Firewall</i>: no. Sadly this isn't a free feature in Cloudflare and since its just a static site with no features, I'm not willing to pay for it.</li>
      </ul>

      <p><u>Security</u></p>
      <p>HTTPS</p>
      <ul>
        <li><i>An SSL Certificate.</i></li>
        <li><i>Re-direct all HTTP requests to HTTPS</i>: so users can't force the site to load under HTTP.</li>
        <li><i>Turn on HTTP Strict Transport Security (HSTS)</i>: works the same way as re-directing users from HTTP to HTTPS.</li>
        <li><i>Automatic HTTPS re-writes</i>: re-writes all URL's using HTTP in my website to HTTPS.</li>
      </ul>

      <p>AWS hardening.</p>
      <ul>
        <li><i>Non-root IAM user for managing S3</i>: least privileges. The user shouldn't have permissions to services that it doesn't need, like making changes to IAM users and roles.</li>
        <li><i>S3 bucket configuration</i>: public read-only access to my website bucket. Disable public access to all other buckets e.g. logging.</li>
        <li><i>S3 Server Access Logging</i>: turn on logging for all requests made to my website bucket, so I have an audit trail.</li>
        <li><i>S3 bucket public read/write compliance</i>: This feature, provided by AWS Config, evaluates all buckets in my account and tells me which have public read/write permissions turned on. It will re-evaluate access each time a configuration change is made to a bucket, allowing me to keep track of buckets which have accidentally been exposed to the public and to close the security hole. This blog post has steps on how to turn on S3 bucket evaluation: <a href="https://aws.amazon.com/premiumsupport/knowledge-center/flag-buckets-aws-config">S3 bucket evaluation.</a></li>
        <li><i>S3 server-side encryption</i>: enable server-side encryption for my private buckets, so even if someone gains access to them, they would need a key to decrypt and read its contents.</li>
      </ul>

      <p>Security scanning.</p>
      <ul>
        <li><i>Web-application security scans</i>: not at the moment because its just a basic static website.</li>
        <li><i>Static code analysis</i>: not at the moment, since I don't know of any free and lightweight tools which I could integrate into my build pipeline. As an alternative, I could use tools to perform pre-commit checks for security issues such as committing secrets.</li>
        <li><i>Third-party dependency scans</i>: yes, npm provides a great tool, npm audit which will check your node modules for security vulnerabilities and even update the module if an issue is detected. It runs automatically on every npm install.</li>
        <li><i>Infrastructure security scans</i>: no. All my content is hosted on S3. This will change with advanced requirements, where I'll need to have a server to host internal services.</li>
      </ul>

      <p>Future security to think about (advanced requirements)</p>
      <ul>
        <li><i>Authentication</i>: only I should be able to access internal services.</li>
        <li><i>Zone / URL lockdown only to my home IP address</i>: internal services will only be accessible via. specific IP addresses.</li>
        <li><i>Web Application Firewall</i>: I want to block people trying to attack my internal services.</li>
      </ul>

      <p><u>Analytics</u></p>
      <p>Just Google Analytics. But I might remove this and just stick with Cloudflare since I only care about basic metrics.</p>

      <h3 style={{ color: "rgb(85, 85, 85)" }}>Developing the User Interface</h3>

      <p>I'm not a frontend expert, but have been looking forward to building a React app. With the help of Facebook's magic, I can start my own React app with a single command.</p>
      <p>To give my website a seamless, single-page app experience, I decide to use React-Router for routing users across the site. This is simple to set up and only requires me to install the package via. npm. This fantastic article goes over how React-Router works and how to use it in a React app: <a href="https://medium.com/@marcellamaki/a-brief-overview-of-react-router-and-client-side-routing-70eb420e8cde">A brief overview of react-router and client-side routing.</a></p>
      <p>With React-Router set up, all I need to do is to make a few design and content changes until the site looks somewhat presentable.</p>
      <p>This is the code that the site is currently running on: <a href="https://gitlab.com/brianlam38/brian-website">Gitlab: brian's website.</a></p>

      <h3 style={{ color: "rgb(85, 85, 85)" }}>Continuous Integration / Continuous Deployment (CICD)</h3>
      <p>Being able to magically push code from my terminal, automatically build and deploy to S3 is great. For this to happen, I need to choose a CI/CD solution.</p>
      <p>At first, I leaned towards using Atlassian's Bitbucket Pipelines since its what I'm familiar with and what I use for work. But I quickly ran out of "build minutes" which is capped at 50 mins / month (it was kind of my fault though, the deploy script was taking forever to run and I should have timed it out earlier).</p>
      <p>Since I had to pay $$$ to continue deploying my project, I decided to find another solution. That solution was Gitlab. Gitlab is great because it provides up to 2000 mins / month (that's 400x more than Bitbucket...). Even if I run out, it only costs an additional $4 per month as opposed to $10 for Bitbucket.</p>
      <p>I used this great blog post as a guide on how to automate my deployment process on Gitlab: <a href="https://rpadovani.com/aws-s3-gitlab">aws s3 gitlab</a></p>

      <h3 style={{ color: "rgb(85, 85, 85)" }}>D.I.Y approach vs managed service?</h3>
      <p>So was this project worth it in the end? Lets do a price comparison between my DIY approach with a managed service like Squarespace.</p>
      <p>Squarespace</p>
      <ul>
        <li>Subscription: $16 per month (yearly) or $22 month to month</li>
      </ul>
      <p>DIY</p>
      <ul>
        <li>Domain name registration: $17 per year / ~$1.42 per month</li>
        <li>AWS Route 53 Hosted Zone: $0.50 forever</li>
        <li>AWS Simple Storage Service: $0.30 to set up my basic site.</li>
        <li>AWS Config Rules: $2 (S3-public-read) + $2 (S3-public-write).</li>
      </ul>
      <p>Total cost for Squarespace: $16 to $22 per month.</p>
      <p>Total cost for my DIY approach: ~$6.22 per month</p>
      
      <p>To be fair, it took me a good ~10 hours to set everything up, which was mostly spent on debugging the React app and tweaking the design and content of the website. All the infrastructure took barely 15 minutes to set up, thanks to AWS. A service-provider like Squarespace would have great themes for you to choose from already, so you wouldn't have to spend hours on Google learning how to bend JSX and CSS to your will.</p>
      <p>Feel free to send me a message if you're interested in learning more about what I did :)</p>

      <i>FYI I'm not affiliated with Amazon, Cloudflare or Gitlab.</i>
      <br></br>
      <br></br>
      <br></br>
    </div>
  );
}

export default Styles2;