import React from 'react';
import logo from './logo.svg';
import './App.css';

function Styles() {
  return (
    <div>
      <h1>This is an example page to show different HTML elements.</h1>
      <img src={logo} className="App-logo" alt="logo" />
      <h1>H1 example.</h1>
      <h2>H2 example.</h2>
      <h3>H3 example.</h3>
      <p>Normal text example.</p>
      <b>Bold text example.</b>
      <i>Italics text example.</i>
      <code>Code block example.</code>
      <a className="App-link" href="https://reactjs.org">Link Example.</a>
      <ol>
          <li>Item 1</li>
          <li>Item 2</li>
          <li>Item 3</li>
      </ol>
    </div>
  );
}

export default Styles;
