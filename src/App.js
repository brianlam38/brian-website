import React from 'react';
import { Route, NavLink } from "react-router-dom";
import './App.css';

import Home from "./Home";
import Styles from "./Styles";
import CSRF from "./CSRF";
import IDOR from "./IDOR";


function App() {
  return (
      <div>
        {/* Site title */}
        <header className="App-header">
          <NavLink to="/" style={{ textDecoration: 'none' }} className="Socials"><h2>Hi, I'm Brian.</h2></NavLink>
        </header>

        <div className="App">
          {/* List of socials */}
          <nav>
            <NavLink to="/" style={{ textDecoration: 'none' }} className="Projects">
              <code>~/root</code>
            </NavLink>
            <a href="https://brianl.me" style={{ textDecoration: 'none' }} className="Socials">|</a>
            <a href="https://www.linkedin.com/in/brianlamsydney" style={{ textDecoration: 'none' }} target="_blank" rel="noopener noreferrer" className="Socials">LinkedIn</a>
            <a href="https://github.com/brianlam38" style={{ textDecoration: 'none' }} target="_blank" rel="noopener noreferrer" className="Socials">Github</a>
            <a href="https://www.hackthebox.eu/profile/6854" style={{ textDecoration: 'none' }} target="_blank" rel="noopener noreferrer" className="Socials">HackTheBox</a>
            <a href="https://bugcrowd.com/brianpwn" style={{ textDecoration: 'none' }} target="_blank" rel="noopener noreferrer" className="Socials">Bugcrowd</a>
          </nav>

          {/* Horizontal rule */}
          <div><hr></hr></div>

          {/* Routes */}
          <Route exact path="/" component={Home}/>
          <Route path="/index.html" component={Home}/>
          <Route path="/Styles" component={Styles}/>
          <Route path="/CSRF" component={CSRF}/>
          <Route path="/IDOR" component={IDOR}/>
        </div>
      </div>
  );
}

export default App;