import React from 'react';
import { NavLink } from "react-router-dom";
import './App.css';

function Home() {
  return (
    <div>
      {/* About me */}
      <h3 style={{ color: "rgb(85, 85, 85)" }}>Whoami</h3>
      <p>Nice to meet you!</p>
      <p>Currently, I'm a security engineer at Ansarada. I believe in securing products by moving away from the traditional gated approach to security by
        <a href="https://brianl.me/" style={{ textDecoration: 'none' }} className="Projects"> "shifting security left" </a>
        and also taking an offensive approach to security.
      </p>
      <p>I enjoy hacking web applications and infrastructure.</p>
      {/* List of projects */}
      <h3 style={{ color: "rgb(85, 85, 85)" }}>Check out my work</h3>
      {/*<NavLink to="/Styles" style={{ textDecoration: 'none' }} className="Projects">
        <code>$ cd "Building a website the hard way."</code>
      </NavLink>
      <br></br>*/}

      <NavLink to="/CSRF" style={{ textDecoration: 'none' }} className="Projects">
        <code>$ cd "CSRF is bad :("</code>
      </NavLink>
      <br></br>
      <NavLink to="/IDOR" style={{ textDecoration: 'none' }} className="Projects">
        <code>$ cd "IDOR and Security Testing - Ansarada Saigon"</code>
      </NavLink>
      <br></br>
    </div>
  );
}

export default Home;