import React from 'react';
import './App.css';


function IDOR() {
  return (
    <div>
      <h3 style={{ color: "rgb(85, 85, 85)"}}>IDOR + Security Testing Intro</h3>
      <p>A presentation on Insecure Direct Object References and Security Testing for Ansarada's Saigon development team.</p>
      <a href="https://brian-website-public-docs.s3-ap-southeast-2.amazonaws.com/IDOR-Security-Testing.pdf" target="_blank" rel="noopener noreferrer">>> IDOR pdf.</a>
    </div>
  );
}

export default IDOR;