import React from 'react';
import './App.css';


function CSRF() {
  return (
    <div>
      <h3 style={{ color: "rgb(85, 85, 85)"}}>CSRF is bad :(</h3>
      <p>A presentation on Cross-Site Request Forgery (CSRF) for Ansarada's frontend team.</p>
      <a href="https://brian-website-public-docs.s3-ap-southeast-2.amazonaws.com/CSRF.pdf" target="_blank" rel="noopener noreferrer">>> CSRF pdf.</a>
    </div>
  );
}

export default CSRF;